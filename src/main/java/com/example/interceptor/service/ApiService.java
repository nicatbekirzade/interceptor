package com.example.interceptor.service;


import com.example.interceptor.client.Cyber;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ApiService {

    private final Cyber cyber;

    public ResponseEntity<String> cyber(boolean mock) {
        return cyber.cyber(mock);
    }

}
