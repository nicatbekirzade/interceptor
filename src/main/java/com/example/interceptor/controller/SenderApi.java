package com.example.interceptor.controller;


import com.example.interceptor.client.Cyber;
import com.example.interceptor.service.ApiService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/myapi")
public class SenderApi {

    private final ApiService service;

    @GetMapping
    ResponseEntity<String> myapi(@RequestHeader("asanMock") boolean mock) {
        log.info("header: {}", mock);

        log.info("response: {}", service.cyber(mock).getBody());
        return ResponseEntity.ok("sent!");
    }
}
