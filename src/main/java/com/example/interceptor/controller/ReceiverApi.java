package com.example.interceptor.controller;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/receiver")
public class ReceiverApi {

    @GetMapping("/cyber")
    ResponseEntity<String> receive(@RequestHeader("asanMock") boolean mock) {
        log.info("header :{}", mock);
        return ResponseEntity.ok("received");
    }

}
