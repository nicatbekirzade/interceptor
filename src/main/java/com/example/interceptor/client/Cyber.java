package com.example.interceptor.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(value = "Cyber", url = "http://localhost:8080")
public interface Cyber {

    @GetMapping("/api/receiver/cyber")
    ResponseEntity<String> cyber(@RequestHeader("asanMock") boolean mock);


}
